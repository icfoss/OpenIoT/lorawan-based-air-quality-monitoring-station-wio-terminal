# LoRaWAN based Air Quality Monitoring Station (Wio Terminal)
Indoor air pollution is a major health hazard in developing countries. A major source of indoor air pollution is the burning of coal, and biomass including wood, charcoal, dung, or crop residue for heating and cooking. This results in high concentrations of particulate matter and was responsible for severe illness and deaths.
Indoor workplaces are found in many working environments such as offices, sales areas, hospitals, libraries, schools and preschool childcare facilities. At such workplaces, no tasks involving hazardous substances are performed, and they do not include high-noise areas. Nevertheless, employees may feature symptoms belonging to the sick building syndrome such as burning of the eyes, scratchy throat, blocked nose, and headaches. These afflictions often cannot be attributed to a single cause, and require a comprehensive analysis besides the testing of the air quality.

![image1](Images/IMG_2.jpg)

# Key Features

   - Powerful MCU: Microchip ATSAMD51P19 with ARM Cortex-M4F core running at 120MHz
   - Reliable Wireless Connectivity: Equipped with Realtek RTL8720DN, dual-band 2.4Ghz / 5Ghz Wi-Fi
   - Highly Integrated Design: 2.4” LCD Screen, IMU and more practical add-ons housed in a compact enclosure with built-in magnets & mounting holes
   - Differentiated display
        - VOC - Volatile organic Compounds
        - NO2 - Nitrogen Dioxide
        - CO - Carbon Monoxide
        - Temp - Temperature
        - Humi - Humidity
        - Ethyl - Ethyl Alcohols
   - Raspberry Pi 40-pin Compatible GPIO
   - Compatible with over 300 plug&play Grove modules to explore with IoT
   - LoraWAN communication using Wio Terminal Chassis - LoRa-E5
   - Display the values graphically as well
   - Support Arduino, CircuitPython, Micropython, ArduPy(What is ArduPy?), AT Firmware, Visual Studio Code
   - PCB available
   - Enclosure available

# Getting Started

- Make sure that you have a [Wio Terminal](https://wiki.seeedstudio.com/Wio-Terminal-Getting-Started/) and [Wio Terminal LoRaWAN Chassis](https://www.seeedstudio.com/Wio-Terminal-Chassis-LoRa-E5-and-GNSS-p-5053.html)
- Install Arduino IDE.
- Select board : Wio Terminal
- For connecting Wio Terminal to Arduino click [here](https://wiki.seeedstudio.com/Wio-Terminal-Getting-Started/)


# Prerequisites
  - Arduino IDE 1.8.19[Tested]
  - Wio Terminal
  - Wio Terminal Battery Chassis
  - Wio Terminal Chassis - LoRa-E5
  - Grove Multichannel Gas Sensor v2
  - Grove Temperature&Humidity Sensor Pro

# Contributing
Instructions coming up soon.

# License
This project is licensed under the MIT License - see the LICENSE.md file for details

# Troubleshooting

# Acknowledgments
 - [Salman Faris](https://www.hackster.io/Salmanfarisvp/air-quality-monitoring-station-with-wio-terminal-6ef85f)
 - [Jaison Jacob](https://gitlab.com/icfoss/OpenIoT/ohw_air_quality_v1.0)

# Contribute to development

# Changelog
