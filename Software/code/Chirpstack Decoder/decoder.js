function Decode(fPort, bytes, variables) {
    /* Data : voc,co,tem,h,no2,c2h5ch */
    var decodedString = String.fromCharCode.apply(null, bytes);
    var data_array = decodedString.split(",");
    var decoded = {};
    decoded.voc = data_array[0];
    decoded.co = data_array[1];
    decoded.temperature = data_array[2];
    decoded.humidity = data_array[3];
    decoded.no2 = data_array[4];
    decoded.c2h5ch = data_array[5];
    return decoded;
}