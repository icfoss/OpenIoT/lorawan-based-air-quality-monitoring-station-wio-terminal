/*
   Air Quality Terminal - Chirpstack
   Author: Jobin J
   Date: 03/04/2023
   Last Updates: 03/04/2023
*/


#include <TFT_eSPI.h>
#include <Multichannel_Gas_GMXXX.h>
#include <Wire.h>
#include "DHT.h"
#include <LoRaWan.h>


char buffer[256];

GAS_GMXXX<TwoWire> gas;

TFT_eSPI tft;
TFT_eSprite spr = TFT_eSprite(&tft);

/* Data : voc,co,tem,h,no2,c2h5ch */
float voc, co, tem, hum, no2, c2h5ch;
//unsigned int no2, c2h5ch, voc, co;

long lastMsg = 0;
char msg[50];
int value = 0;

#define DHTPIN 0
#define DHTTYPE DHT22

DHT dht(DHTPIN, DHTTYPE);

unsigned long previousMillis = millis();
unsigned long interval =  5000;

unsigned long dataSendPreviousMillis = -1000 * 60 * 15 * 2;
unsigned long dataSendCurrentMillis;
unsigned long dataSendInterval =  1000 * 60 * 15;


void loraSetup() {
  Serial.println("Lora setup started.");
  lora.init();
  memset(buffer, 0, 256);
  lora.getVersion(buffer, 256, 1);
  memset(buffer, 0, 256);
  lora.getId(buffer, 256, 1);

  //lora.setId(char *DevAddr, char *DevEUI, char *AppEUI);
  lora.setId("0x00000000", "XXXXXXXXXXXXXXXX", "XXXXXXXXXXXXXXXX");
  //void setKey(char *NwkSKey, char *AppSKey, char *AppKey);
  lora.setKey("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");


  //lora.setDeciveMode(LWABP);
  lora.setDeciveMode(LWOTAA);
  lora.setDataRate(DR0, IN865);

  lora.setDutyCycle(false);
  lora.setJoinDutyCycle(false);

  lora.setPower(14);
  while (!lora.setOTAAJoin(JOIN));
  Serial.println("Lora setup done!");
}

void setup() {
  Serial.begin(9600);

  tft.begin();
  tft.setRotation(3);

  gas.begin(Wire, 0x08);

  dht.begin();

  // Head
  tft.fillScreen(TFT_BLACK);
  tft.setFreeFont(&FreeSansBoldOblique18pt7b);
  tft.setTextColor(TFT_WHITE);
  tft.drawString("Air Quality", 70, 10 , 1);

  // Line
  for (int8_t line_index = 0; line_index < 5 ; line_index++) {
    tft.drawLine(0, 50 + line_index, tft.width(), 50 + line_index, TFT_GREEN);
  }


  // VCO & CO Box
  tft.drawRoundRect(5, 60, (tft.width() / 2) - 20 , tft.height() - 65 , 10, TFT_WHITE); // L1

  // VCO Text
  tft.setFreeFont(&FreeSansBoldOblique12pt7b);
  tft.setTextColor(TFT_RED);
  tft.drawString("VOC", 7 , 65 , 1);
  tft.setTextColor(TFT_GREEN);
  tft.drawString("ppm", 55, 108, 1);

  // CO Text
  tft.setFreeFont(&FreeSansBoldOblique12pt7b);
  tft.setTextColor(TFT_RED);
  tft.drawString("CO", 7 , 150 , 1);
  tft.setTextColor(TFT_GREEN);
  tft.drawString("ppm", 55, 193, 1);


  // Temp Box
  tft.drawRoundRect((tft.width() / 2) - 10  , 60, (tft.width() / 2) / 2 , (tft.height() - 65) / 2 , 10, TFT_BLUE); // s1

  tft.setFreeFont(&FreeSansBoldOblique9pt7b);
  tft.setTextColor(TFT_RED) ;
  tft.drawString("Temp", (tft.width() / 2) - 1  , 70 , 1); // Print the test text in the custom font
  tft.setTextColor(TFT_GREEN);
  tft.drawString("o", (tft.width() / 2) + 30, 95, 1);
  tft.drawString("C", (tft.width() / 2) + 40, 100, 1);


  // No2 Box
  tft.drawRoundRect(((tft.width() / 2) + (tft.width() / 2) / 2) - 5  , 60, (tft.width() / 2) / 2 , (tft.height() - 65) / 2 , 10, TFT_BLUE); // s2

  tft.setFreeFont(&FreeSansBoldOblique9pt7b);
  tft.setTextColor(TFT_RED);
  tft.drawString("NO2", ((tft.width() / 2) + (tft.width() / 2) / 2)   , 70 , 1); // Print the test text in the custom font
  tft.setTextColor(TFT_GREEN);
  tft.drawString("ppm", ((tft.width() / 2) + (tft.width() / 2) / 2) + 30 , 120, 1);

  // Humi Box
  tft.drawRoundRect((tft.width() / 2) - 10 , (tft.height() / 2) + 30, (tft.width() / 2) / 2 , (tft.height() - 65) / 2 , 10, TFT_BLUE); // s3

  tft.setFreeFont(&FreeSansBoldOblique9pt7b);
  tft.setTextColor(TFT_RED) ;
  tft.drawString("Humi", (tft.width() / 2) - 1 , (tft.height() / 2) + 40 , 1); // Print the test text in the custom font
  tft.setTextColor(TFT_GREEN);
  tft.drawString("%", (tft.width() / 2) + 30, (tft.height() / 2) + 70, 1);



  // c2h5ch Box
  tft.drawRoundRect(((tft.width() / 2) + (tft.width() / 2) / 2) - 5  , (tft.height() / 2) + 30, (tft.width() / 2) / 2 , (tft.height() - 65) / 2 , 10, TFT_BLUE); // s4


  tft.setFreeFont(&FreeSansBoldOblique9pt7b);
  tft.setTextColor(TFT_RED) ;
  tft.drawString("Ethyl", ((tft.width() / 2) + (tft.width() / 2) / 2)   , (tft.height() / 2) + 40 , 1); // Print the test text in the custom font
  tft.setTextColor(TFT_GREEN);
  tft.drawString("ppm", ((tft.width() / 2) + (tft.width() / 2) / 2) + 30 , (tft.height() / 2) + 90, 1);

  loraSetup();
}

void loop() {

  dataSendCurrentMillis = millis();

  // VOC
  voc = gas.getGM502B();
  if (voc > 999) voc = 999;
  Serial.print("VOC: ");
  Serial.print(voc);
  Serial.print(" ppm  ");
  voc = voc / 10;


  spr.createSprite(40, 30);
  spr.fillSprite(TFT_BLACK);
  spr.setFreeFont(&FreeSansBoldOblique12pt7b);
  spr.setTextColor(TFT_WHITE);
  spr.drawNumber(int(voc), 0, 0, 1);
  spr.pushSprite(15, 100);
  spr.deleteSprite();

  // CO
  co = gas.getGM702B();
  if (co > 999) co = 999;
  Serial.print("CO: ");
  Serial.print(co);
  Serial.print(" ppm  ");
  co = co / 22;

  spr.createSprite(40, 30);
  spr.setFreeFont(&FreeSansBoldOblique12pt7b);
  spr.setTextColor(TFT_WHITE);
  spr.drawNumber(int(co), 0, 0, 1);
  spr.setTextColor(TFT_GREEN);
  spr.pushSprite(15, 185);
  spr.deleteSprite();

  // Temp
  tem = dht.readTemperature();
  Serial.print("Temperature: ");
  Serial.print(tem);
  Serial.print( "*C   ");

  spr.createSprite(30, 30);
  spr.setFreeFont(&FreeSansBoldOblique12pt7b);
  spr.setTextColor(TFT_WHITE);
  spr.drawNumber(int(tem), 0, 0, 1);
  spr.setTextColor(TFT_GREEN);
  spr.pushSprite((tft.width() / 2) - 1, 100);
  spr.deleteSprite();

  // NO2
  no2 = gas.getGM102B();
  if (no2 > 999) no2 = 999;
  Serial.print("NO2: ");
  Serial.print(int(no2));
  Serial.print(" ppm  ");
  no2 = no2 / 33;

  spr.createSprite(45, 30);
  spr.setFreeFont(&FreeSansBoldOblique12pt7b);
  spr.setTextColor(TFT_WHITE);
  spr.drawNumber(no2, 0, 0, 1);
  spr.pushSprite(((tft.width() / 2) + (tft.width() / 2) / 2), 97);
  spr.deleteSprite();


  // Humidity
  hum = dht.readHumidity();
  if (hum > 99) hum = 99;
  Serial.print("Humidity: ");
  Serial.print(hum);
  Serial.print( "%  ");

  spr.createSprite(30, 30);
  spr.setFreeFont(&FreeSansBoldOblique12pt7b);
  spr.setTextColor(TFT_WHITE);
  spr.drawNumber(int(hum), 0, 0, 1);
  spr.pushSprite((tft.width() / 2) - 1, (tft.height() / 2) + 67);
  spr.deleteSprite();

  // C2H5CH
  c2h5ch = gas.getGM302B();
  if (c2h5ch > 999) c2h5ch = 999;
  Serial.print("C2H5CH: ");
  Serial.print(c2h5ch);
  Serial.println(" ppm  ");
  c2h5ch = c2h5ch / 6.5;

  spr.createSprite(45, 30);
  spr.setFreeFont(&FreeSansBoldOblique12pt7b);
  spr.setTextColor(TFT_WHITE);
  spr.drawNumber(int(c2h5ch), 0 , 0, 1);
  spr.pushSprite(((tft.width() / 2) + (tft.width() / 2) / 2), (tft.height() / 2) + 67);
  spr.deleteSprite();


  // Send data
  if (dataSendCurrentMillis - dataSendPreviousMillis >= dataSendInterval) {
    // reset counter time
    dataSendPreviousMillis = dataSendCurrentMillis;
    bool result = false;
    char json_data[20];
    // voc,co,tem,h,no2,c2h5ch
    char voc_[6], co_[6], tem_[6], hum_[6], no2_[6], c2h5ch_[6];
    dtostrf(voc, 4, 2, voc_);
    dtostrf(co, 4, 2, co_);
    dtostrf(tem, 4, 2, tem_);
    dtostrf(hum, 4, 2, hum_);
    dtostrf(no2, 4, 2, no2_);
    dtostrf(c2h5ch, 4, 2, c2h5ch_);

    sprintf(json_data, "%s,%s,%s,%s,%s,%s", voc_, co_, tem_, hum_, no2_, c2h5ch_);
    char *buffer_data = (char *)json_data;
    Serial.println(buffer_data);
    result = lora.transferPacket(buffer_data, 12);
    Serial.println(result);

    if (result) {
      short length;
      short rssi;

      memset(buffer, 0, 256);
      length = lora.receivePacket(buffer, 256, &rssi);

      if (length)
      {
        SerialUSB.print("Length is: ");
        SerialUSB.println(length);
        SerialUSB.print("RSSI is: ");
        SerialUSB.println(rssi);
        SerialUSB.print("Data is: ");
        for (unsigned char i = 0; i < length; i ++)
        {
          SerialUSB.print("0x");
          SerialUSB.print(buffer[i], HEX);
          SerialUSB.print(" ");
        }
        SerialUSB.println();
      }
    }
  }
  delay(3000);
}
