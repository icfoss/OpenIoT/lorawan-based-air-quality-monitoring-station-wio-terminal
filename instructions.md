## Download and install Arduino IDE

https://support.arduino.cc/hc/en-us/articles/360019833020-Download-and-install-Arduino-IDE

## Get Started with Wio Terminal

https://wiki.seeedstudio.com/Wio-Terminal-Getting-Started/

## Add Libraries
```c
Grove_Temperature_And_Humidity_Sensor
Seeed_Arduino_MultiGas
```

## Configure LoraWAN credentials in main code
```c
//lora.setId(char *DevAddr, char *DevEUI, char *AppEUI);
lora.setId("0x00000000", "XXXXXXXXXXXXXXXX", "XXXXXXXXXXXXXXXX");
//void setKey(char *NwkSKey, char *AppSKey, char *AppKey);
lora.setKey("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
```

## You can find a 'Wire.cpp' file in the packages folder for Seeduino. Comment on these lines before compiling this project.

```c
  #if defined(__SAMD51__)
    void WIRE1_IT_HANDLER_0(void) { Wire1.onService(); }
    void WIRE1_IT_HANDLER_1(void) { Wire1.onService(); }
    void WIRE1_IT_HANDLER_2(void) { Wire1.onService(); }
    void WIRE1_IT_HANDLER_3(void) { Wire1.onService(); }
  #endif // __SAMD51__
```

